import click
import os
import random


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def write(input_path: str, output_path: str):
    """Функция write создает новый файл из
    исходного файла путем подбора строк
    случайным образом"""
    with open(input_path, mode="r", encoding="utf-8") as sourcefile:
        lines = sourcefile.readlines()
        with open(output_path, mode="at", encoding="utf-8") as myfile:
            size = 0
            maximum_size = 1024 * 1024
            while size < maximum_size:
                myline = random.choice(lines)
                size = os.path.getsize(input_path)
                myfile.write(myline)


if __name__ == '__main__':
    write()
