import click
import os


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
@click.argument('result_size', type=click.INT)
def multiply(input_path: str, output_path: str, result_size: int):
    """Функция multiply создает новый файл
    заданного размера из исходного файла
    путем копирования. В качестве аргумента
    принимает название результирующего
    файла и его размер"""
    with open(output_path, mode="wb") as resultfile:
        size = os.path.getsize(output_path)
        while size < result_size:
            with open(input_path, mode="rb") as myfile:
                resultfile.write(myfile.read())
            size = os.path.getsize(output_path)


if __name__ == '__main__':
    multiply()
