from typing import Dict

import click
import re


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def count_frequency(input_path: str, output_path: str):
    """Функция count_frequency подсчитывает
    частоту встречаемости слов в исходном
    текстовом файле. В выходной файл функция
    пишет список пар слово и число
    повторений."""
    frequency: Dict[str, int]
    frequency = {}
    with open(input_path, mode="r", encoding='utf-8') as sourcefile:
        text_string = sourcefile.read().lower()
        match_pattern = re.findall(r"\b[a-zA-Zа-яА-Я]"
                                   r"{3,15}\b",
                                   text_string)
        for word in match_pattern:
            count = frequency.get(word, 0)
            frequency[word] = count + 1
        frequency_list = frequency.keys()
    with open(output_path, mode="w", encoding='utf-8') as myfile:
        for words in frequency_list:
            tup = words, frequency[words]
            myline = " ".join(str(x) for x in tup)
            myfile.write(myline)


if __name__ == '__main__':
    count_frequency()
