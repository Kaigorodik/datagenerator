#!/usr/bin/env python
# coding: utf-8

# In[18]:


import io
import os
import random
import re


class File(io.FileIO):

    __size = 0
    size1MB = 1024 * 1024
    size10MB = 1024 * 1024 * 10
    size100MB = 1024 * 1024 * 100
    size1GB = 1024 * 1024 * 1024
    size5GB = 1024 * 1024 * 1024 * 5
    size10GB = 1024 * 1024 * 1024 * 10

    def __init__(
        self, name, namesource, maximum_size,
            mode="r", closefd=True, opener=None
    ):
        io.FileIO.__init__(self, name, mode, closefd, opener)
        self.maximum_size = maximum_size
        self.__size = os.path.getsize(name)
        self.namesource = namesource

    def write(self):
        """Функция write создает новый файл из
        исходного файла путем подбора строк
        случайным образом"""
        with open(self.namesource, mode="r", encoding="utf-8") as sourcefile:
            lines = sourcefile.readlines()
        with open(self.name, mode="at", encoding="utf-8") as myfile:
            size = self.__size
            while size < self.maximum_size:
                myline = random.choice(lines)
                size = os.path.getsize(self.name)
                myfile.write(myline)

    def multiply(self, result, result_size):
        """Функция multiply создает новый файл
        заданного размера из исходного файла
        путем копирования. В качестве аргумента
        принимает название результирующего
        файла и его размер"""
        with open(result, mode="wb") as resultfile:
            size = os.path.getsize(result)
            while size < result_size:
                with open(self.name, mode="rb") as myfile:
                    resultfile.write(myfile.read())
                size = os.path.getsize(result)

    def count_frequency(self):
        """Функция count_frequency подсчитывает
        частоту встречаемости слов в исходном
        текстовом файле. В выходной файл функция
        пишет список пар слово и число
        повторений."""
        frequency = {}
        with open(self.namesource, mode="r") as sourcefile:
            text_string = sourcefile.read().lower()
            match_pattern = re.findall(r"\b[a-zA-Zа-яА-Я]"
                                       r"{3,15}\b",
                                       text_string)
            for word in match_pattern:
                count = frequency.get(word, 0)
                frequency[word] = count + 1
            frequency_list = frequency.keys()
        with open(self.name, mode="w") as myfile:
            for words in frequency_list:
                tup = words, frequency[words]
                myline = " ".join(str(x) for x in tup)
                myfile.write(myline)
