import src

SOURCE_DATA_PATH = 'data/raw/shagren.txt'
WRITE_DATA_PATH = 'data/interim/result1MB.txt'
MULTIPLY_DATA_PATH = 'data/interim/result10MB.txt'
COUNT_FREQUENCY_DATA_PATH = 'data/processed/count_result10MB.txt'
size10MB: int = 1024 * 1024 * 10

if __name__ == '__main__':
    src.write(SOURCE_DATA_PATH, WRITE_DATA_PATH)
    src.multiply(WRITE_DATA_PATH, MULTIPLY_DATA_PATH, size10MB)
    src.count_frequency(MULTIPLY_DATA_PATH, COUNT_FREQUENCY_DATA_PATH)
